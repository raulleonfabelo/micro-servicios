﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace WebApiProject.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductController : ControllerBase
    {
        [HttpGet]
        public ActionResult<List<Product>> GetAll()
        {
            return Products;
        }

        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            return Products.Single(p => p.Id == id);
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            product.Id = Products.Count() + 1;
            Products.Add(product);
            return CreatedAtAction(
                "Get",
                new { id = product.Id },
                product
                );
        }

        [HttpPut("{productId}")]
        public ActionResult Update(int productId, Product product)
        {
            var originalProdut = Products.Single(p => p.Id == productId);

            originalProdut.Name = product.Name;
            originalProdut.Price = product.Price;
            originalProdut.Description = product.Description;

            return NoContent();
        }

        [HttpDelete("{productId}")]
        public ActionResult Delete(int productId)
        {
            Products.RemoveAll(p => p.Id == productId);

            return NoContent();
        }

        private static List<Product> Products = new List<Product> {
            new Product{
                Id = 1,
                Name = "p1",
                Description = "d1",
                Price = 1
            },
            new Product{
                Id = 2,
                Name = "p2",
                Description = "d2",
                Price = 2 }
        };
    }
}
